<?php
use Zend\Db\Adapter;
use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;

return [
 'service_manager' => [
  'abstract_factories' => [
   Adapter\AdapterAbstractServiceFactory::class,
  ],
  'factories' => [
   Adapter\AdapterInterface::class => Adapter\AdapterServiceFactory::class,
  ],
  'aliases' => [
      Adapter\Adapter::class => Adapter\AdapterInterface::class,
  ],
 ],
        // Session configuration.
    'session_config' => [
        'cookie_lifetime'     => 60*60*1, // Session cookie will expire in 1 hour.
        'gc_maxlifetime'      => 60*60*24*30, // How long to store session data on server (for 1 month).        
    ],
    // Session manager configuration.
    'session_manager' => [
        // Session validators (used for security).
        'validators' => [
            RemoteAddr::class,
            HttpUserAgent::class,
        ]
    ],
    // Session storage configuration.
    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],
    'doctrine' => [        
        // migrations configuration
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
 'db' => [
       'driver' => 'Mysqli',
 'database' => 'zendusers',
    'username' => 'root',
    'password' => '98789878s',
  /*'driver' => 'Mysqli',
  'dsn' => 'mysql:dbname=zendusers;hostname=localhost;port=3306',
  'driver_options' => [
   1002 => 'SET NAMES \'UTF8\''
  ],*/
 ]
];