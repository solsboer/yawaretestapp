<?php
namespace Acl;

///use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Router\Http\Literal;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'controllers' => [
        'factories' => [
            Controller\AclController::class => Controller\AclControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ],
            ],
        ],
    ], 
    'router' => [
        'routes' => [
            'Acl' => [
                'type'    => Literal::class,
                'options' => [
                    // Change this to something specific to your module
                    'route'    => '/acl',
                    'defaults' => [
                        'controller'    => Controller\AclController::class,
                        'action'        => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    // You can place additional routes that match under the
                    // route defined above here.
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_map' =>[
'acl/acl/index' => __DIR__.'/../view/acl/acl/index.phtml',
],
        'template_path_stack' => [
             __DIR__ . '/../view',
        ],
    ],
          'service_manager' => [
        'factories' => [
            Entity\Permissions::class => Entity\Factory\PermissionsFactory::class,
            Entity\Roles::class => Entity\Factory\RolesFactory::class,
           // Entity\UserManager::class => Entity\Factory\UserManagerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            'Acl_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_default' => [
                'drivers' => [
                    'Acl\Entity' => 'Acl_driver'
                ]
            ]
        ],
    ],
];
