<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acl\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

use Acl\Entity\Permissions;
use Acl\Entity\Roles;

/**
 * Description of AclControllerFactory
 *
 * @author sergiy
 */
class AclControllerFactory implements FactoryInterface {
    //put your code here
 public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
    $entity = $container->get("doctrine.entitymanager.orm_default");
  //  $roles = $container->get(Roles::class);
   // $permissions = $container->get(Permissions::class);
    return new AclController($entity);//,$roles,$permissions);
    }

}
