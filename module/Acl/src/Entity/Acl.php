<?php
namespace Acl\Entity;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Stdlib\Exception\InvalidArgumentException;
use Zend\Stdlib\Exception\RuntimeException;
/**
 * Description of Acl
 *
 * @author sergiy
 */
class Acl  extends ZendAcl{
    protected $acl;
    protected $resources =[];
    protected $roles =[];
    protected $permissions=[];
    
    public function __construct(array $roles,array $resources,array $permissions)
    {
        $this->roles = $roles;
        $this->resources = $resources;
        $this->permissions = $permissions;
        
         $this->loadRoles()
             ->loadResources()
             ->loadPrivileges();
    }
     /**
     * Load Roles from factory
     * @return $this
     */
    protected function loadRoles()
    {
        foreach($this->roles as $role) {
            if($role->getParent())
                $this->addRole(new Role($role->getName()), new Role($role->getParent()->getName()));
            else
                $this->addRole(new Role($role->getName()));
            if($role->getDeveloper())
                $this->allow($role->getName(), array(), array());
        }
        return $this;
    }
    /**
     * Load Resources from factory
     * @return $this
     */
    protected function loadResources()
    {
        foreach($this->resources as $resource)
            $this->addResource(new Resource($resource->getName()));
        return $this;
    }
    /**
     * Load Privileges from factory
     * @return $this
     */
    protected function loadPrivileges()
    {
        /*foreach($this->permissions as $privilege) {
        //   $this->addResource(new Resource('home'));
            $this->allow('guest', ['Application\Controller\IndexController','News\Controller\NewsController','User\Controller\AuthController','Register\Controller\SkeletonController','News\Controller\TagController' ], ['index','login','logout']);
            $this->allow('employee', ['User\Controller\UserController'],['index']);
            $this->allow('admin', 'Acl\Controller\AclController');
            $this->allow('admin', 'User\Controller\UserController','add');
            $this->allow('owner', array(), array());
           /* $this->allow('admin', 'Acl\Controller\AclController', array());
             $this->allow('employee', ['User\Controller\UserController','Register\Controller\Index'], array());
             $this->allow('guest', ['Application\Controller\IndexController','User\Controller\AuthController'], array());
            */
            
             /* All actions from resource or just the actions that are allowed to the role */
           foreach($this->permissions as $privilege) {
            if($privilege->getPermissions() == 'All') {
                $this->allow($privilege->getRole()->getName(), $privilege->getResource()->getName(), array());
            } else {
                $actions = json_decode($privilege->getPermissions(), true);
                $this->allow($privilege->getRole()->getName(), $privilege->getResource()->getName(),$actions);
                
            }
      
        }
        return $this;
    }
    /**
     * @param  Zend\Permissions\Acl\Role\RoleInterface|string $role
     * @param  Zend\Permissions\Acl\Resource\ResourceInterface|string $resource
     * @param  string $privilege
     * @return bool
     */
    public function isAllowed($role = null, $resource = null, $privilege = null)
    {
        if (!$this->hasRole($role)) {
            return false;
        }
        if (!$this->hasResource($resource)) {
            return false;
        }
        return parent::isAllowed($role, $resource, $privilege);
    }
}

