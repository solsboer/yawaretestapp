<?php
namespace Acl\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Description of Permissions
 *
 * @author sergiy
 */
/**
 * This class represents a permission of user.
 * @ORM\Entity()
 * @ORM\Table(name="acl_permissions")
 */
class Permissions {
         /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
         protected $id;
         /** 
     * @ORM\Column(name="permissions", type="text")  
     */
    protected $permissions;
         /**
     * @var \Acl\Entity\Roles
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $role;
          /**
     * @var \Acl\Entity\Resources
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\Resources")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resource_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $resource;

   public function getId(){
        return $this->id;
    }
    public function getPermissions() {
        return $this->permissions;   
    }
   public function getRole(){
       return $this->role;
   }
   public function getResource(){
       return $this->resource;
   }
    
}
