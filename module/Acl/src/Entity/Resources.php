<?php
namespace Acl\Entity;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use \User\Entity\User;
use Acl\Entity\Permissions;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acl\Entity\Resources
 *
 * @ORM\Entity
 * @ORM\Table(name="acl_resources")
 */
class Resources{
    
// private $resources;
      
       /**
     * @var string
     * @ORM\Column(name="name")
     */
     private $name;
     
       /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
     private $id;
      /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return Resource
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

  }   
  