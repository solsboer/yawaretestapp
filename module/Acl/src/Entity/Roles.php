<?php
namespace Acl\Entity;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use \User\Entity\User;
use Acl\Entity\Permissions;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity
 * @ORM\Table(name="acl_roles")
 */
class Roles{
       /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
       protected $id;
              /**
     * @var string
     * @ORM\Column(name="name")
     */
     private $name;
     
        /**
     * @var \Acl\Entity\AclRole
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $parent;
    private $owner = false;
    
     public function getId(){
        return $this->id;
    }
   public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
     public function setParent(\Acl\Entity\Roles $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }
    /**
     * Get parent
     *
     * @return \Acl\Entity\Role 
     */
    public function getParent()
    {
        return $this->parent;
    }
     public function getDeveloper()
    {
        if($this->name == 'developer')return true;
    }
    /*
     * toArray
     */
    public function toArray()
    {
        if(isset($this->parent))
            $parent = $this->parent->getId();
        else 
            $parent = false;
        
        return array(
            'id'        => $this->id,
            'name'      => $this->name,
            'layout'    => $this->layout,
            'redirect'  => $this->redirect,
            'parent'    => $parent
        );
    }

}