<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Acl;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
     public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Entity\Acl::class => function($container) {
                    /* Get ACL from database */
					$em         = $container->get(\Doctrine\ORM\EntityManager::class);
					$roles      = $em->getRepository(\Acl\Entity\Roles::class)->findAll();
					$resources  = $em->getRepository(\Acl\Entity\Resources::class)->findAll();
					$privileges = $em->getRepository(\Acl\Entity\Permissions::class)->findAll();
                    return new Entity\Acl($roles, $resources, $privileges);
                }
            )
        );
    }
}
