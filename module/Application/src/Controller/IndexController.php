<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
class IndexController extends AbstractActionController
{
     private $config;
    // private $db;
    public function __construct(array $config) {
        $this->config = $config;
       // $this->db = new \Zend\Db\Adapter\Adapter($config);
    }
    public function indexAction()
    {

         
       return new ViewModel();
  
    
    }
}
