<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Description of AclControllerFactory
 *
 * @author sergiy
 */
class IndexControllerFactory implements FactoryInterface {
    //put your code here
 public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
    $config = $container->get("Config");
    return new IndexController($config['db']);
    }

}
