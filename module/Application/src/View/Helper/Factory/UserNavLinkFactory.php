<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Application\View\Helper\Factory;



use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Description of UserNavLinkFactory
 *
 * @author sergiy
 */
class UserNavLinkFactory implements FactoryInterface {
 
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
                $authenticationService = $container->get(\Zend\Authentication\AuthenticationService::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
 return new \Application\View\Helper\UserNavLink($authenticationService,$entityManager);
        
    }//put your code here
}
