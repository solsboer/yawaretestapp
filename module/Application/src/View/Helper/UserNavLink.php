<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use User\Entity\User;
/**
 * Show user in nav menu
 *        $authenticationService = $container->get(\Zend\Authentication\AuthenticationService::class);

 * @author sergiy
 */
class UserNavLink extends AbstractHelper {
          /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    private $authService;
    protected $user;
    protected $name = 'Guest';
      public function __construct($auth,$em) 
    {
        $this->authService = $auth;
        $this->entityManager = $em;
        if($this->authService->hasIdentity())
          $this->user = $this->entityManager->getRepository(User::class)
                ->findOneByEmail($this->authService->getIdentity());
       if($this->user != null) $this->name = $this->user->getLogin();
    }
    public function showUserNavLink(){
        $escapeHtml = $this->getView()->plugin('escapeHtml');
        $result = '';/*
        $result .= '<div class="dropdown">';
        $result .= '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
            <span class="glyphicon glyphicon-user"></span>Hi,'.$this->name;
        $result .= ' <span class="caret"></span>';
        $result .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
        //  $result .=   '<li class="nav-item"><a>Hi,'.$this->name.'</a></li>';
        $result .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Дія</a></li>';
        $result .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша дія</a></li>';
        $result .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Щось ще тут</a></li>';
        $result .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Відокремлений лінк</a></li>';
        $result .= '</ul>';
        $result .= '</div>';
        echo $result;*/
        $result .= '<ul class="nav navbar-nav navbar-right">';
        $result .= '<li class = "dropdown">';
        $result .= '<a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">';
        if ($this->name != 'Guest') {
            $result .= '<span class="glyphicon glyphicon-user"></span>';
            $result .= ' Hi, ' . $this->name . ' <b class = "caret"></b></a>';
            $result .= '<ul class = "dropdown-menu">';
            $result .= '<li><a href = "' . $escapeHtml('user'). '">My Team</a></li>';
            $result .= '<li role="presentation" class="divider"></li>';
            $result .= '<li><a href = "' . $escapeHtml('/logout') . '">Logout</a></li>';
        } else {
            $result .= '<span class="glyphicon glyphicon-log-in"></span>';
            $result .= ' Login <b class = "caret"></b></a>';
            $result .= '<ul class = "dropdown-menu">';
            $result .= '<li><a href = "' . $escapeHtml('/login') . '">Login</a></li>';
            $result .= '<li><a href = "' . $escapeHtml('/register') . '">Register</a></li>';
        }
        $result .= '</ul>';
        $result .= '</li>';
        $result .= '</ul>';
        echo $result;
    }
}
