<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;
use User\Form\UserForm;
use User\Form\RegisterForm;

class UserController extends AbstractActionController
{
        /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    /**
     * User manager.
     * @var User\Service\UserManager 
     */
    private $userManager;
    private $AuthManager;
    private $me;
     /**
     * Constructor. 
     */
    public function __construct($entityManager, $userManager,$authManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        //$container = new Container();
        $this->AuthManager = $authManager;
         $this->me = $this->entityManager->getRepository(User::class)
                ->findOneByEmail($this->AuthManager->getIdentity());
       
    }
    public function indexAction()
    {
        $this->me = $this->entityManager->getRepository(User::class)
                ->findOneByEmail($this->AuthManager->getIdentity());
        $users = $this->entityManager->getRepository(User::class)
                ->findBy(['parent' => $this->me->getParent()]);
        return new ViewModel([
            'user' => $users
        ]);
    }

    public function addAction() {
        // Create user form
        if ($this->me->getRole()->getName() == 'admin') {
            $form = new UserForm('create', true, $this->entityManager);
        } else {
            $form = new UserForm('create', false, $this->entityManager);
        }


        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid()) {
                echo 'form is valid';
                // Get filtered and validated data
                $data = $form->getData();

                // Add user.
                //If admin adds user set parent to admin parent
                if ($this->me->getRole()->getName() == 'admin') {
                    $user = $this->userManager->addUser($data, $this->me->getParent());
                } else {
                    $user = $this->userManager->addUser($data, $this->me);
                }

                // Redirect to "view" page
                return $this->redirect()->toRoute('User');
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function editAction() 
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $user = $this->entityManager->getRepository(User::class)
                ->find($id);
        if($user->getRole()->getDeveloper() || $user->getRole()->getName() == 'owner')
        	throw new \Exception('You cant edit higher role.');
        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        // Create user form
        if($this->me->getRole()->getName() == 'admin'){
            $form = new UserForm('update',true, $this->entityManager);
        } else {
            $form = new UserForm('update',false, $this->entityManager);
        }
       
        
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                var_dump($data);
                // Get filtered and validated data
                $data = $form->getData();
               var_dump($data);
                // Update the user.
                $this->userManager->updateUser($user, $data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('User', 
                        ['action'=>'index']);                
            }               
        } else {
            $form->setData(array(
                'login' => $user->getLogin(),
                'email'=>$user->getEmail(),
                'role' => $user->getRole(),
                'status' => $user->getStatus(),
            ));
        }
        
        return new ViewModel(array(
            'user' => $user,
            'form' => $form
        ));
    }
    
  public function registerAction() {
      if($this->me != NULL)
          throw new \Exception('You already registered. For register as new user pleace logout.');
      $form = new RegisterForm();
       // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $this->userManager->registerUser($data);
                // Redirect to "view" page
                return $this->redirect()->toRoute('login');
            }
        }
         return new ViewModel(array(
            'form' => $form
        ));
      
  }
    
}
