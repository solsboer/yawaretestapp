<?php
namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity()
 * @ORM\Table(name="Users")
 */
class User 
{
    // User status constants.
    const STATUS_ACTIVE       = 1; // Active user.
    const STATUS_RETIRED      = 2; // Retired user.
    
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** 
     * @ORM\Column(name="Email")  
     */
    protected $email;
    
    /** 
     * @ORM\Column(name="Login")  
     */
    protected $login;

    /** 
     * @ORM\Column(name="Password")  
     */
    protected $password;
    
   /** 
     * @ORM\Column(name="Status")  
     */
    protected $status;
      /**
     * @var \Acl\Entity\Roles
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role", referencedColumnName="id", nullable=false)
     * })
     */
    private $role;
    
         /**
     * @var \Ac
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Parent_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $parent;
    

    /**
     * Returns user ID.
     * @return integer
     */
    public function getId() 
    {
        return $this->id;
    }

    /**
     * Sets user ID. 
     * @param int $id    
     */
    public function setId($id) 
    {
        $this->id = $id;
    }

    /**
     * Returns email.     
     * @return string
     */
    public function getEmail() 
    {
        return $this->email;
    }

    /**
     * Sets email.     
     * @param string $email
     */
    public function setEmail($email) 
    {
        $this->email = $email;
    }
    
    /**
     * Returns full name.
     * @return string     
     */
    public function getLogin() 
    {
        return $this->login;
    }       

    /**
     * Sets full name.
     * @param string $login
     */
    public function setLogin($login) 
    {
        $this->login = $login;
    }
   /**
     * Returns status.
     * @return int     
     */
    public function getParent() 
    {
        return $this->parent;
    }
   /**
     * Sets status.
     * @param int $parent     
     */
    public function setParent($parent) 
    {
        $this->parent = $parent;
    }   
    /**
     * Returns status.
     * @return int     
     */
    public function getRole() 
    {
        return $this->role;
    }

  
    /**
     * @param \Acl\Entity\Roles $role
     * @return this    
     */
    public function setRole(\Acl\Entity\Roles $role = null) 
    {
        $this->role = $role;
        return $this;
    }   
    
    /**
     * Returns password.
     * @return string
     */
    public function getPassword() 
    {
       return $this->password; 
    }
    
    /**
     * Sets password.     
     * @param string $password
     */
    public function setPassword($password) 
    {
        $this->password = $password;
    }
       
    /**
     * Sets status.
     * @param int $status     
     */
    public function setStatus($status = 1) 
    {
        $this->status = $status;
    }  
        /**
     * Returns status.
     * @return int     
     */
    public function getStatus() 
    {
        return $this->status;
    }

   
}



