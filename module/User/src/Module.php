<?php

namespace User;

use User\Controller\Factory\AuthControllerFactory;
use User\Controller\AuthController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Mvc\MvcEvent;
use User\Entity\User;



class Module implements ConfigProviderInterface, ServiceProviderInterface, ControllerProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $container = $e->getApplication()->getServiceManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function (MvcEvent $e) use ($container) {
            $match = $e->getRouteMatch();
            $authService = $container->get(AuthenticationServiceInterface::class);
            $routeName = $match->getMatchedRouteName();
            $em = $container->get(\Doctrine\ORM\EntityManager::class);
            /* Get Controller and Action */
            $matchedController = $match->getParam('controller');
            $matchedAction = $match->getParam('action');

            /* Default Role */
            $role = 'guest';
            /* Check if user exists, if it has authenticated and set role */
            if ($authService->hasIdentity()) {
                $user = $em->getRepository(User::class)
                        ->findOneByEmail($authService->getIdentity());
                if (is_object($user)) {
                    $role = $user->getRole()->getName();
                } else {

                    $match->setParam('controller', AuthController::class)
                            ->setParam('action', 'logout');
                    print_r('Please, first LogIn');
                }
            }
            /* Valid ACL */
            $acl = $container->get(\Acl\Entity\Acl::class);

            if (!$acl->isAllowed($role, $matchedController, $matchedAction)) {
                if ($role == 'guest' && $routeName != 'login') {
                    echo '  Please, First LogIn';
                    $match->setParam('controller', AuthController::class)
                            ->setParam('action', 'login');
                } else {
                    
                    $response = $e->getResponse();
                    /* Location to page or whatever */
                    $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
                    $response->setStatusCode(303);
                    
                }
            }
        }, 100);
    }

    public function getConfig() {
        return include __DIR__ . "/../config/module.config.php";
    }

    public function getServiceConfig() {
        return [
            'aliases' => [
                AuthenticationService::class => AuthenticationServiceInterface::class
            ],
            'factories' => [
                AuthenticationServiceInterface::class => Service\Factory\AuthenticationServiceFactory::class
            ]
        ];
    }

    public function getControllerConfig() {
        return [
            'factories' => [
                AuthController::class => AuthControllerFactory::class,
                \Acl\Controller\AclController::class => \Acl\Controller\AclControllerFactory::class,
            ]
        ];
    }

}
