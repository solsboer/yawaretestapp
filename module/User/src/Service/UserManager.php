<?php
namespace User\Service;



use User\Entity\User;
use Zend\Crypt\Password\Bcrypt;
use Zend\Math\Rand;

/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class UserManager
{
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($entityManager) 
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new user.
     */
    public function addUser($data,$parent) 
    {
     
        // Do not allow several users with the same email address.
        if($this->checkUserExists($data['email'])) {
            throw new \Exception("User with email address " . $data['$email'] . " already exists");
        }
                 $role = $this->entityManager->getRepository(\Acl\Entity\Roles::class)
                ->findOneBy(['id' => $data['role']]);
       
        // Create new User entity.
        $user = new User();
        $user->setEmail($data['email']);
        $user->setLogin($data['login']);        

        // Encrypt password and store the password in encrypted state.
       // $bcrypt = new Bcrypt();
       // $passwordHash = $bcrypt->create($data['password']);        
        $user->setPassword($data['password']);
        $user->setRole($role);
        $user->setStatus($data['status']);
        $user->setParent($parent);
      
                
        // Add the entity to the entity manager.
        $this->entityManager->persist($user);
        
        // Apply changes to database.
        $this->entityManager->flush();
        
        return $user;
    }
    
    /**
     * This method updates data of an existing user.
     */
    public function updateUser($user, $data) 
    {
        //  Do not allow to change user email if another user with such email already exits.
        if ($user->getEmail() != $data['email'] && $this->checkUserExists($data['email'])) {
            throw new \Exception("Another user with email address " . $data['email'] . " already exists");
        }
        $role = $this->entityManager->getRepository(\Acl\Entity\Roles::class)
                ->findOneBy(['id' => $data['role']]);
        $user->setEmail($data['email']);

        $user->setLogin($data['login']);
        $user->setStatus($data['status']); 
        $user->setRole($role);
        
        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }
    
    /**
     * This method checks if at least one user presents, and if not, creates 
     * 'Admin' user with email 'admin@example.com' and password 'Secur1ty'. 
     */
    public function createAdminUserIfNotExists()
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([]);
        if ($user==null) {
            $user = new User();
            $user->setEmail('admin@example.com');
            $user->setLogin('Admin');
            $bcrypt = new Bcrypt();
            $passwordHash = $bcrypt->create('Secur1ty');        
            $user->setPassword($passwordHash);
           // $user->setStatus(User::STATUS_ACTIVE);
           // $user->setDateCreated(date('Y-m-d H:i:s'));
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
    
    /**
     * Checks whether an active user with given email address already exists in the database.     
     */
    public function checkUserExists($email) {
        
        $user = $this->entityManager->getRepository(User::class)
                ->findOneByEmail($email);
        
        return $user !== null;
    }
    public function registerUser($data) {
           //  Do not allow to change user email if another user with such email already exits.
        if ($this->checkUserExists($data['email'])) {
            throw new \Exception("Another user with email address " . $data['email'] . " already exists");
        }
         // Create new User entity.
        $user = new User();
        $user->setEmail($data['email']);
        $user->setLogin($data['login']);               
        $user->setPassword($data['password']);
        $role = $this->entityManager->getRepository(\Acl\Entity\Roles::class)->findOneBy(['id'=>'4']);
        $user->setRole($role);
        $user->setStatus();
        $user->setParent($user);
   
        // Add the entity to the entity manager.
        $this->entityManager->persist($user);
        
        // Apply changes to database.
        $this->entityManager->flush();
        
    }
}

